%% Practicas de Matlab
%% Resolución de EDO con métodos monopaso
%% Hoja 3
% *Nombre: Ostin Antenor* 
% 
% *Apellido: Llantoy Salvatierra* 
% 
% *DNI: 09822482X*
%% 
% 
%% 1. Implementación de métodos explícitos
% Práctica 1 (Implementación del método de Euler explícito) 
% Escribir en el Apéndice A1 una función implementando el método de Euler (explícito) 
% 
% $$      \left\{\begin{array}{l}               y_{i+1}=y_i + h f(t_i,y_i) \quad 
% i=0,\ldots ,N-1          \\               y_0 \approx a        \end{array}$$
% 
% para el PVI (problema de valor inicial para sistemas de EDOs) y que responda 
% a la sintaxis
% 
% |[t,y]=mieuler(f,intv,y0,N)|
% 
% El pseudocódigo correspondiente se encuentra en el CV (campus virtual). 
% Práctica 2 (Implementación del método de Euler modificado explícito) 
% Escribir en el Apéndice A1 una función que implemente el método de Euler modificado 
% (explícito) 
% 
% $$\begin{array}{ccl}  y_{i+1} &=& y_i + h f\left(t_i + \frac{h}{2}, y_i + 
% \frac{h}{2} f(t_i,y_i)\right), \quad  i=0,\ldots ,N-1 \\  y_0 &\approx& a\end{array}$$
% 
% para el PVI (problema de valor inicial para sistemas de EDOs) y que responda 
% a la sintaxis
% 
% |[t,y]=mieulermod(f,intv,y0,N)| 
% Práctica 3 (Implementación del método de Euler mejorado explícito) 
% Escribir en el Apéndice A1 una función que implemente el método de Euler mejorado 
% (explícito) 
% 
% $$\begin{array}{ccl}y_{i+1} &=& y_i +  \left.{h\over 2} (f(t_i,y_i) + f(t_{i+1},  
% y_i+hf(t_i,y_i)\right), \quad i=0,\ldots ,N-1\\ y_0 &\approx& a\end{array}$$
% 
% para el PVI (problema de valor inicial para sistemas de EDOs) y que responda 
% a la sintaxis
% 
% |[t,y]=mieulermej(f,intv,y0,N)| 
% Práctica 4 (Implementación del método de Runge-Kutta explícito) 
% Escribir en el Apéndice A1 una función que implemente el método de Euler mejorado 
% (explícito) 
% 
% $$    \begin{array}{ccl}      y_{i+1} &=& y_i + h \Phi(t_i,y_i,h), \quad i=0,\ldots 
% ,N-1 \\      y_0 &\approx& a    \end{array}$$
% 
% donde $\Phi(t,y,h)=\frac{1}{6}\left(F_1+2F_2+2F_3+F_4\right)$ y 
% 
% $$    \begin{array}{l}      F_1=f(t,y)\\      F_2=f\left(t+\frac{h}{2},y+\frac{h}{2}F_1\right)\\      
% F_3=f\left(t+\frac{h}{2},y+\frac{h}{2}F_2\right) \\      F_4=f\left(t+h,y+hF_3\right),    
% \end{array}$$
% 
% para el PVI (problema de valor inicial para sistemas de EDOs) y que responda 
% a la sintaxis
% 
% |[t,y]=mirk4(f,intv,y0,N)|
% Práctica 5 (EDO de corazón) 
% Considera el siguiente PVI
% 
% $$    \begin{array}{ccc}    \frac{dx_1}{dt} & = & x_2                 \\    
% \frac{dx_2}{dt} & = & -16x_1 + 4 \sin(2t) \\    x_1(0)          & = & 0                  
% \\    x_2(0)          & = & 2    \end{array}$$
% 
% en el intervalo, $[0,2 \pi]$.  Ahora intenta resolverla numéricamente usando
%% 
% # el método de Euler $N=100,400,800$
% # el método de Euler modificado
% # el método de Euler mejorado 
% # el método de Runge Kutta 4 
%% 
% pinta el diagrama de fases.
% 
% *Solución*
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 

disp('Hoja 3: 5 Ostin Antenor Llantoy Salvatierra')
fun=@f;

%% Método de Euler

% N=100
[t,A]=mieuler(fun,[0,2*pi],[0,2],100);
v1=A(1,:);
v2=A(2,:);
plot(v1,v2,'r*');
title({'Diagrama de fase (corazón)';'met=mieuler, intv=[0,2*pi], x0=[0,2], N=100'})

% N=400
[t,A]=mieuler(fun,[0,2*pi],[0,2],400);
v1=A(1,:);
v2=A(2,:);
plot(v1,v2,'r*');
title({'Diagrama de fase (corazón)';'met=mieuler, intv=[0,2*pi], x0=[0,2], N=400'})

% N=800 
[t,A]=mieuler(fun,[0,2*pi],[0,2],800);
v1=A(1,:);
v2=A(2,:);
plot(v1,v2,'r*');
title({'Diagrama de fase (corazón)';'met=mieuler, intv=[0,2*pi], x0=[0,2], N=800'})


%% Método de Euler modificado 

[t,A]=mieulermod(fun,[0,2*pi],[0,2],100);
v1=A(1,:);
v2=A(2,:);
plot(v1,v2,'r+');
title({'Diagrama de fase (corazón)';'met=mieulermod, intv=[0,2*pi], x0=[0,2], N=100'})


%% Método de Euler mejorado 

[t,A]=mieulermej(fun,[0,2*pi],[0,2],100);
v1=A(1,:);
v2=A(2,:);
plot(v1,v2,'r<-');
title({'Diagrama de fase (corazón)';'met=mieulermej, intv=[0,2*pi], x0=[0,2], N=100'})


%% Método de Runge-Kutta

[t,A]=mirk4(fun,[0,2*pi],[0,2],100);
v1=A(1,:);
v2=A(2,:);
plot(v1,v2,'r-');
title({'Diagrama de fase (corazón)';'met=mirk4, intv=[0,2*pi], x0=[0,2], N=100'})

%%
function [x]=f(tv,yv)
x(1)=yv(2);
x(2)=-16*yv(1)+4*sin(2*tv);
end

 
%% Apéndice código: funciones de Euler, Euler modificado, Euler mejorado y Runge-Kutta 4


%%%%%%%%%%%%% 

function [t,A]=mieuler(f,intv,y0,N)

disp('Hoja 5: func: Euler Ostin Antenor Llantoy Salvatierra');

h=(intv(2)-intv(1))/N;

t(1)=0;
for i=1:N-1
    t(i+1)=t(i)+h;
end

y0=transpose(y0);
A(:,1)=y0;
for i=1:N-1
    A(:,i+1)=A(:,i)+h*transpose(f(t(i),transpose(A(:,i))));
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%


function [t,A]=mieulermod(f,intv,y0,N)
disp('Hoja 5: func: Euler modificado Ostin Antenor Llantoy Salvatierra')


h=(intv(2)-intv(1))/N;

t(1)=0;
for i=1:N-1
    t(i+1)=t(i)+h;
end

y0=transpose(y0);
A(:,1)=y0;
for i=1:N-1
    A(:,i+1)=A(:,i)+h*transpose(f(t(i)+(h/2),transpose(A(:,i)+(h/2)*transpose(f(t(i),transpose(A(:,i)))))));
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5


function [t,A]=mieulermej(f,intv,y0,N)
disp('Hoja 5: func: Euler mejorado Ostin Antenor Llantoy Salvatierra')


h=(intv(2)-intv(1))/N;

t(1)=0;
for i=1:N-1
    t(i+1)=t(i)+h;
end

y0=transpose(y0);
A(:,1)=y0;
for i=1:N-1
    A(:,i+1)=A(:,i)+(h/2)*transpose(f(t(i),transpose(A(:,i)))+(f(t(i+1),A(:,i)+h*transpose(f(t(i),transpose(A(:,i)))))));
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [t,A]=mirk4(f,intv,y0,N)
disp('Hoja 5: func: Runge-Kutta Ostin Antenor Llantoy Salvatierra')

h=(intv(2)-intv(1))/N;

t(1)=0;
for i=1:N-1
    t(i+1)=t(i)+h;
end

    function[valF1]=F1(t,y)
        valF1=f(t,y);
    end
    function[valF2]=F2(t,y)
        valF2=f(t+(h/2),y+(h/2)*F1(t,y));
    end
    function[valF3]=F3(t,y)
        valF3=f(t+(h/2),y+(h/2)*F2(t,y));
    end
    function[valF4]=F4(t,y)
        valF4=f(t+h,y+h*F3(t,y));
    end


y0=transpose(y0);
A(:,1)=y0;
for i=1:N-1
    A(:,i+1)=A(:,i)+h*(1/6)*(transpose(F1(t(i),transpose(A(:,i))))+2*transpose(F2(t(i),transpose(A(:,i))))+2*transpose(F3(t(i),transpose(A(:,i))))+transpose(F4(t(i),transpose(A(:,i)))));
end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%










%% 
% 
% 
% 
% 
% etc
